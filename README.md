# README #

This project is a Liferay 6.2 portlet

### What is this repository for? ###

To calculate the user score.

## Requirements  
| Framework                                                                        |Version  |License             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|
|[Java SE Development Kit](https://www.java.com/it/download/help/index_installing.xml?j=7) |7.0 |[Oracle Binary Code License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)         |Also v.8|
|[Liferay Portal CE bundled with Tomcat](https://sourceforge.net/projects/lportal/files/Liferay%20Portal/6.2.3%20GA4/liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip/download) |6.2 GA4 | [GNU LGPL](https://www.liferay.com/it/downloads/ce-license)      ||
|[postgreSQL](https://www.postgresql.org/download/)| 9.3  |[PostgreSQL License](https://www.postgresql.org/about/licence/) ||
|[Social Office CE](https://web.liferay.com/it/community/liferay-projects/liferay-social-office/overview) | 3.0 | GNU LGPL |

## Libraries

Liferay Theme is based on the following software libraries and frameworks.

|Framework                           |Version       |Licence                                      |
|------------------------------------|--------------|---------------------------------------------|
|[jersey-client](https://jersey.java.net) |1.8 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|
|[jersey-core](https://jersey.java.net) |1.8 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|
|[Jersey core common](https://jersey.java.net) |2.1 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|

## Dependencies
|Library                             |Project       |License                                      |
|------------------------------------|--------------|---------------------------------------------|
|Challenge62-portlet-service.jar| welive-oia | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|MarketplaceStore-portlet-service.jar| welive-mkt| [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|


### How do I get set up? ###

For details about configuration/installation you can see "D2.4 – WELIVE OPEN INNOVATION AND CROWDSOURCING TOOLS V2"

Remember to set the username/password for the Basic Authentication on the file \Reputazione-utente-portlet\docroot\WEB-INF\src\portlet.properties

### Who do I talk to? ###

ENG team, Filippo Giuffrida, Antonino Sirchia

### Copying and License

This code is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)