/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.reputation.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author asirchia
 * @generated
 */
public class ReputationSoap implements Serializable {
	public static ReputationSoap toSoapModel(Reputation model) {
		ReputationSoap soapModel = new ReputationSoap();

		soapModel.setReputationId(model.getReputationId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setReputationScore(model.getReputationScore());
		soapModel.setModifiedDate(model.getModifiedDate());

		return soapModel;
	}

	public static ReputationSoap[] toSoapModels(Reputation[] models) {
		ReputationSoap[] soapModels = new ReputationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ReputationSoap[][] toSoapModels(Reputation[][] models) {
		ReputationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ReputationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ReputationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ReputationSoap[] toSoapModels(List<Reputation> models) {
		List<ReputationSoap> soapModels = new ArrayList<ReputationSoap>(models.size());

		for (Reputation model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ReputationSoap[soapModels.size()]);
	}

	public ReputationSoap() {
	}

	public long getPrimaryKey() {
		return _reputationId;
	}

	public void setPrimaryKey(long pk) {
		setReputationId(pk);
	}

	public long getReputationId() {
		return _reputationId;
	}

	public void setReputationId(long reputationId) {
		_reputationId = reputationId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public int getReputationScore() {
		return _reputationScore;
	}

	public void setReputationScore(int reputationScore) {
		_reputationScore = reputationScore;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	private long _reputationId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private int _reputationScore;
	private Date _modifiedDate;
}