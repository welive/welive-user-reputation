/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.reputation.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Reputation}.
 * </p>
 *
 * @author asirchia
 * @see Reputation
 * @generated
 */
public class ReputationWrapper implements Reputation, ModelWrapper<Reputation> {
	public ReputationWrapper(Reputation reputation) {
		_reputation = reputation;
	}

	@Override
	public Class<?> getModelClass() {
		return Reputation.class;
	}

	@Override
	public String getModelClassName() {
		return Reputation.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("reputationId", getReputationId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("reputationScore", getReputationScore());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long reputationId = (Long)attributes.get("reputationId");

		if (reputationId != null) {
			setReputationId(reputationId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Integer reputationScore = (Integer)attributes.get("reputationScore");

		if (reputationScore != null) {
			setReputationScore(reputationScore);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	/**
	* Returns the primary key of this reputation.
	*
	* @return the primary key of this reputation
	*/
	@Override
	public long getPrimaryKey() {
		return _reputation.getPrimaryKey();
	}

	/**
	* Sets the primary key of this reputation.
	*
	* @param primaryKey the primary key of this reputation
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_reputation.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the reputation ID of this reputation.
	*
	* @return the reputation ID of this reputation
	*/
	@Override
	public long getReputationId() {
		return _reputation.getReputationId();
	}

	/**
	* Sets the reputation ID of this reputation.
	*
	* @param reputationId the reputation ID of this reputation
	*/
	@Override
	public void setReputationId(long reputationId) {
		_reputation.setReputationId(reputationId);
	}

	/**
	* Returns the group ID of this reputation.
	*
	* @return the group ID of this reputation
	*/
	@Override
	public long getGroupId() {
		return _reputation.getGroupId();
	}

	/**
	* Sets the group ID of this reputation.
	*
	* @param groupId the group ID of this reputation
	*/
	@Override
	public void setGroupId(long groupId) {
		_reputation.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this reputation.
	*
	* @return the company ID of this reputation
	*/
	@Override
	public long getCompanyId() {
		return _reputation.getCompanyId();
	}

	/**
	* Sets the company ID of this reputation.
	*
	* @param companyId the company ID of this reputation
	*/
	@Override
	public void setCompanyId(long companyId) {
		_reputation.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this reputation.
	*
	* @return the user ID of this reputation
	*/
	@Override
	public long getUserId() {
		return _reputation.getUserId();
	}

	/**
	* Sets the user ID of this reputation.
	*
	* @param userId the user ID of this reputation
	*/
	@Override
	public void setUserId(long userId) {
		_reputation.setUserId(userId);
	}

	/**
	* Returns the user uuid of this reputation.
	*
	* @return the user uuid of this reputation
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputation.getUserUuid();
	}

	/**
	* Sets the user uuid of this reputation.
	*
	* @param userUuid the user uuid of this reputation
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_reputation.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this reputation.
	*
	* @return the user name of this reputation
	*/
	@Override
	public java.lang.String getUserName() {
		return _reputation.getUserName();
	}

	/**
	* Sets the user name of this reputation.
	*
	* @param userName the user name of this reputation
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_reputation.setUserName(userName);
	}

	/**
	* Returns the reputation score of this reputation.
	*
	* @return the reputation score of this reputation
	*/
	@Override
	public int getReputationScore() {
		return _reputation.getReputationScore();
	}

	/**
	* Sets the reputation score of this reputation.
	*
	* @param reputationScore the reputation score of this reputation
	*/
	@Override
	public void setReputationScore(int reputationScore) {
		_reputation.setReputationScore(reputationScore);
	}

	/**
	* Returns the modified date of this reputation.
	*
	* @return the modified date of this reputation
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _reputation.getModifiedDate();
	}

	/**
	* Sets the modified date of this reputation.
	*
	* @param modifiedDate the modified date of this reputation
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_reputation.setModifiedDate(modifiedDate);
	}

	@Override
	public boolean isNew() {
		return _reputation.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_reputation.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _reputation.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_reputation.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _reputation.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _reputation.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_reputation.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _reputation.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_reputation.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_reputation.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_reputation.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ReputationWrapper((Reputation)_reputation.clone());
	}

	@Override
	public int compareTo(it.eng.reputation.model.Reputation reputation) {
		return _reputation.compareTo(reputation);
	}

	@Override
	public int hashCode() {
		return _reputation.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.reputation.model.Reputation> toCacheModel() {
		return _reputation.toCacheModel();
	}

	@Override
	public it.eng.reputation.model.Reputation toEscapedModel() {
		return new ReputationWrapper(_reputation.toEscapedModel());
	}

	@Override
	public it.eng.reputation.model.Reputation toUnescapedModel() {
		return new ReputationWrapper(_reputation.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _reputation.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _reputation.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_reputation.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReputationWrapper)) {
			return false;
		}

		ReputationWrapper reputationWrapper = (ReputationWrapper)obj;

		if (Validator.equals(_reputation, reputationWrapper._reputation)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Reputation getWrappedReputation() {
		return _reputation;
	}

	@Override
	public Reputation getWrappedModel() {
		return _reputation;
	}

	@Override
	public void resetOriginalValues() {
		_reputation.resetOriginalValues();
	}

	private Reputation _reputation;
}