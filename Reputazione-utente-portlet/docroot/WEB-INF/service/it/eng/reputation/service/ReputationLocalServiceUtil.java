/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.reputation.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Reputation. This utility wraps
 * {@link it.eng.reputation.service.impl.ReputationLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author asirchia
 * @see ReputationLocalService
 * @see it.eng.reputation.service.base.ReputationLocalServiceBaseImpl
 * @see it.eng.reputation.service.impl.ReputationLocalServiceImpl
 * @generated
 */
public class ReputationLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.reputation.service.impl.ReputationLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the reputation to the database. Also notifies the appropriate model listeners.
	*
	* @param reputation the reputation
	* @return the reputation that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.reputation.model.Reputation addReputation(
		it.eng.reputation.model.Reputation reputation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addReputation(reputation);
	}

	/**
	* Creates a new reputation with the primary key. Does not add the reputation to the database.
	*
	* @param reputationId the primary key for the new reputation
	* @return the new reputation
	*/
	public static it.eng.reputation.model.Reputation createReputation(
		long reputationId) {
		return getService().createReputation(reputationId);
	}

	/**
	* Deletes the reputation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reputationId the primary key of the reputation
	* @return the reputation that was removed
	* @throws PortalException if a reputation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.reputation.model.Reputation deleteReputation(
		long reputationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteReputation(reputationId);
	}

	/**
	* Deletes the reputation from the database. Also notifies the appropriate model listeners.
	*
	* @param reputation the reputation
	* @return the reputation that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.reputation.model.Reputation deleteReputation(
		it.eng.reputation.model.Reputation reputation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteReputation(reputation);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.reputation.model.Reputation fetchReputation(
		long reputationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchReputation(reputationId);
	}

	/**
	* Returns the reputation with the primary key.
	*
	* @param reputationId the primary key of the reputation
	* @return the reputation
	* @throws PortalException if a reputation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.reputation.model.Reputation getReputation(
		long reputationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getReputation(reputationId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the reputations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reputations
	* @param end the upper bound of the range of reputations (not inclusive)
	* @return the range of reputations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.reputation.model.Reputation> getReputations(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getReputations(start, end);
	}

	/**
	* Returns the number of reputations.
	*
	* @return the number of reputations
	* @throws SystemException if a system exception occurred
	*/
	public static int getReputationsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getReputationsCount();
	}

	/**
	* Updates the reputation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param reputation the reputation
	* @return the reputation that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.reputation.model.Reputation updateReputation(
		it.eng.reputation.model.Reputation reputation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateReputation(reputation);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<it.eng.reputation.model.Reputation> getReputationByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getReputationByUserId(userId);
	}

	public static java.util.List<it.eng.reputation.model.Reputation> getOrderedReputation() {
		return getService().getOrderedReputation();
	}

	public static void clearService() {
		_service = null;
	}

	public static ReputationLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ReputationLocalService.class.getName());

			if (invokableLocalService instanceof ReputationLocalService) {
				_service = (ReputationLocalService)invokableLocalService;
			}
			else {
				_service = new ReputationLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ReputationLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(ReputationLocalService service) {
	}

	private static ReputationLocalService _service;
}