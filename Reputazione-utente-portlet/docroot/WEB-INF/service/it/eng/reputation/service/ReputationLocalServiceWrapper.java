/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.reputation.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ReputationLocalService}.
 *
 * @author asirchia
 * @see ReputationLocalService
 * @generated
 */
public class ReputationLocalServiceWrapper implements ReputationLocalService,
	ServiceWrapper<ReputationLocalService> {
	public ReputationLocalServiceWrapper(
		ReputationLocalService reputationLocalService) {
		_reputationLocalService = reputationLocalService;
	}

	/**
	* Adds the reputation to the database. Also notifies the appropriate model listeners.
	*
	* @param reputation the reputation
	* @return the reputation that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.reputation.model.Reputation addReputation(
		it.eng.reputation.model.Reputation reputation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.addReputation(reputation);
	}

	/**
	* Creates a new reputation with the primary key. Does not add the reputation to the database.
	*
	* @param reputationId the primary key for the new reputation
	* @return the new reputation
	*/
	@Override
	public it.eng.reputation.model.Reputation createReputation(
		long reputationId) {
		return _reputationLocalService.createReputation(reputationId);
	}

	/**
	* Deletes the reputation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reputationId the primary key of the reputation
	* @return the reputation that was removed
	* @throws PortalException if a reputation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.reputation.model.Reputation deleteReputation(
		long reputationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.deleteReputation(reputationId);
	}

	/**
	* Deletes the reputation from the database. Also notifies the appropriate model listeners.
	*
	* @param reputation the reputation
	* @return the reputation that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.reputation.model.Reputation deleteReputation(
		it.eng.reputation.model.Reputation reputation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.deleteReputation(reputation);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _reputationLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.reputation.model.Reputation fetchReputation(long reputationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.fetchReputation(reputationId);
	}

	/**
	* Returns the reputation with the primary key.
	*
	* @param reputationId the primary key of the reputation
	* @return the reputation
	* @throws PortalException if a reputation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.reputation.model.Reputation getReputation(long reputationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.getReputation(reputationId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the reputations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reputations
	* @param end the upper bound of the range of reputations (not inclusive)
	* @return the range of reputations
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.reputation.model.Reputation> getReputations(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.getReputations(start, end);
	}

	/**
	* Returns the number of reputations.
	*
	* @return the number of reputations
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getReputationsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.getReputationsCount();
	}

	/**
	* Updates the reputation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param reputation the reputation
	* @return the reputation that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.reputation.model.Reputation updateReputation(
		it.eng.reputation.model.Reputation reputation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.updateReputation(reputation);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _reputationLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_reputationLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _reputationLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<it.eng.reputation.model.Reputation> getReputationByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _reputationLocalService.getReputationByUserId(userId);
	}

	@Override
	public java.util.List<it.eng.reputation.model.Reputation> getOrderedReputation() {
		return _reputationLocalService.getOrderedReputation();
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ReputationLocalService getWrappedReputationLocalService() {
		return _reputationLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedReputationLocalService(
		ReputationLocalService reputationLocalService) {
		_reputationLocalService = reputationLocalService;
	}

	@Override
	public ReputationLocalService getWrappedService() {
		return _reputationLocalService;
	}

	@Override
	public void setWrappedService(ReputationLocalService reputationLocalService) {
		_reputationLocalService = reputationLocalService;
	}

	private ReputationLocalService _reputationLocalService;
}