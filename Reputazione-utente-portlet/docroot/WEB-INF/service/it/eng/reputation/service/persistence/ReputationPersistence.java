/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.reputation.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.reputation.model.Reputation;

/**
 * The persistence interface for the reputation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author asirchia
 * @see ReputationPersistenceImpl
 * @see ReputationUtil
 * @generated
 */
public interface ReputationPersistence extends BasePersistence<Reputation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ReputationUtil} to access the reputation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the reputations where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching reputations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.reputation.model.Reputation> findByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the reputations where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of reputations
	* @param end the upper bound of the range of reputations (not inclusive)
	* @return the range of matching reputations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.reputation.model.Reputation> findByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the reputations where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of reputations
	* @param end the upper bound of the range of reputations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reputations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.reputation.model.Reputation> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first reputation in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reputation
	* @throws it.eng.reputation.NoSuchReputationException if a matching reputation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.reputation.model.Reputation findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.reputation.NoSuchReputationException;

	/**
	* Returns the first reputation in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching reputation, or <code>null</code> if a matching reputation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.reputation.model.Reputation fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last reputation in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reputation
	* @throws it.eng.reputation.NoSuchReputationException if a matching reputation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.reputation.model.Reputation findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.reputation.NoSuchReputationException;

	/**
	* Returns the last reputation in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching reputation, or <code>null</code> if a matching reputation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.reputation.model.Reputation fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the reputations before and after the current reputation in the ordered set where userId = &#63;.
	*
	* @param reputationId the primary key of the current reputation
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next reputation
	* @throws it.eng.reputation.NoSuchReputationException if a reputation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.reputation.model.Reputation[] findByUserId_PrevAndNext(
		long reputationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.reputation.NoSuchReputationException;

	/**
	* Removes all the reputations where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of reputations where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching reputations
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the reputation in the entity cache if it is enabled.
	*
	* @param reputation the reputation
	*/
	public void cacheResult(it.eng.reputation.model.Reputation reputation);

	/**
	* Caches the reputations in the entity cache if it is enabled.
	*
	* @param reputations the reputations
	*/
	public void cacheResult(
		java.util.List<it.eng.reputation.model.Reputation> reputations);

	/**
	* Creates a new reputation with the primary key. Does not add the reputation to the database.
	*
	* @param reputationId the primary key for the new reputation
	* @return the new reputation
	*/
	public it.eng.reputation.model.Reputation create(long reputationId);

	/**
	* Removes the reputation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reputationId the primary key of the reputation
	* @return the reputation that was removed
	* @throws it.eng.reputation.NoSuchReputationException if a reputation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.reputation.model.Reputation remove(long reputationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.reputation.NoSuchReputationException;

	public it.eng.reputation.model.Reputation updateImpl(
		it.eng.reputation.model.Reputation reputation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the reputation with the primary key or throws a {@link it.eng.reputation.NoSuchReputationException} if it could not be found.
	*
	* @param reputationId the primary key of the reputation
	* @return the reputation
	* @throws it.eng.reputation.NoSuchReputationException if a reputation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.reputation.model.Reputation findByPrimaryKey(
		long reputationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.reputation.NoSuchReputationException;

	/**
	* Returns the reputation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param reputationId the primary key of the reputation
	* @return the reputation, or <code>null</code> if a reputation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.reputation.model.Reputation fetchByPrimaryKey(
		long reputationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the reputations.
	*
	* @return the reputations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.reputation.model.Reputation> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the reputations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reputations
	* @param end the upper bound of the range of reputations (not inclusive)
	* @return the range of reputations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.reputation.model.Reputation> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the reputations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reputations
	* @param end the upper bound of the range of reputations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of reputations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.reputation.model.Reputation> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the reputations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of reputations.
	*
	* @return the number of reputations
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}