create table reputation_Reputation (
	reputationId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	reputationScore INTEGER,
	modifiedDate DATE null
);