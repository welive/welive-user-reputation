package com.test;

import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.MediaType;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.util.portlet.PortletProps;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public abstract class CDVNotifier {

	private static JSONObject wrappertemplate;
	private static JSONObject  template;
	
	static{
		wrappertemplate = JSONFactoryUtil.createJSONObject();
		wrappertemplate.put("eventName", "ReputationUpdated");
		wrappertemplate.put("entries", JSONFactoryUtil.createJSONArray());
		
		template = JSONFactoryUtil.createJSONObject();
		template.put("key", "newReputation");
	}
	
	private static JSONArray createMessage(Map<User, Double> scores){
		
		JSONArray ext = JSONFactoryUtil.createJSONArray();
		for(Entry<User,Double> score:scores.entrySet()){
			try {
				JSONObject messageWrapper = JSONFactoryUtil.createJSONObject(wrappertemplate.toString());
				PermissionChecker checker = PermissionCheckerFactoryUtil.create(score.getKey());
				PermissionThreadLocal.setPermissionChecker(checker);
				messageWrapper.put("ccUserID", Integer.parseInt(score.getKey().getExpandoBridge().getAttribute("CCUserID").toString()));
				JSONArray entries = messageWrapper.getJSONArray("entries");
			
				JSONObject message = JSONFactoryUtil.createJSONObject(template.toString());
				message.put("value", score.getValue());
				entries.put(message);
				
				ext.put(messageWrapper);
			} 
			catch (Exception e) { e.printStackTrace(); }
		}
		
		return ext;
	}
	
	public static void notify(Map<User, Double> scores){
		if(!Boolean.parseBoolean(PortletProps.get("cdv-enabled")))
			return;
		
		
		JSONArray message = createMessage(scores);
		
		Client client = Client.create();
		String username = PortletProps.get("remoteservice.user");
		String password = PortletProps.get("remoteservice.pass");
		
		client.addFilter(new HTTPBasicAuthFilter(username, password));
		WebResource webResource = client.resource(PortletProps.get("cdv-url"));
        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, message.toString());
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
		System.out.println(resp);
	}
	
}
