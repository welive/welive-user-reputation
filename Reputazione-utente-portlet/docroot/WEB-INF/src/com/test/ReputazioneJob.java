package com.test;

import it.eng.reputation.model.Reputation;
import it.eng.reputation.model.impl.ReputationImpl;
import it.eng.reputation.service.ReputationLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaProblemRelevant;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaSolutionRealistic;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaSolveProblem;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.marketplace.artefatto.model.Artefatto;
import it.eng.rspa.marketplace.artefatto.service.ArtefattoLocalServiceUtil;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBDiscussion;
import com.liferay.portlet.messageboards.model.MBMessage;
import com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil;
import com.liferay.portlet.ratings.model.RatingsEntry;
import com.liferay.portlet.ratings.service.RatingsEntryLocalServiceUtil;
import com.liferay.portlet.ratings.service.RatingsStatsLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

public class ReputazioneJob implements MessageListener{
	
	private List<User> getAllUsers() throws SystemException {
		List<User> users = null;
		users = UserLocalServiceUtil.getUsers(0, UserLocalServiceUtil.getUsersCount());
		return users;
	}
	
	private List<User> getClassifiableUsers() throws SystemException, PortalException{
		List<User> out = new ArrayList<User>();
		
		List<User> all = getAllUsers();
		long companyId = CompanyLocalServiceUtil.getCompanyByWebId(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID)).getCompanyId();
		Role role_admin = RoleLocalServiceUtil.getRole(companyId, PortletProps.get("ruolo_admin"));
		
		for(User u : all) {
			if(!u.isActive()) {
				//Non valuta gli utenti inattivi
				continue; 
			}
			else if(u.isDefaultUser()) {
				//Non valuta l'utente di default
				continue;
			}
			else if(RoleLocalServiceUtil.getUserRoles(u.getUserId()).contains(role_admin)) {
				//Non valuta l'utente amministratore
				continue; 
			}
			else {
				out.add(u);
			}
		}
		
		return out;
	}

	private List<CLSIdea> getIdeaAuthor(long userId) throws SystemException {
		return CLSIdeaLocalServiceUtil.getIdeasByUserId(userId);
	}
	
	private List<CLSChallenge> getChallengesAuthor(long userId) throws SystemException {
		return CLSChallengeLocalServiceUtil.getChallengesByUserId(userId);
	}
	
	private double getChallengesContribution(List<CLSChallenge> challenges) throws NumberFormatException, SystemException {
		
		double acc = 0.0;
		
		//Aggiorno la reputazione sulla base del numero di gare trovate
		acc += Double.parseDouble(PortletProps.get("my-challenges-weight"))*challenges.size();
		
		for(CLSChallenge c : challenges) {
			try {
				//Aggiorno la reputazione sulla base del numero di idee pubblicate sulle gare dell'utente corrente
				acc += Double.parseDouble(PortletProps.get("ideas-for-my-challenges-weight"))*CLSIdeaLocalServiceUtil.getIdeasByChallengeId(c.getChallengeId()).size();
				
				//Aggiorno la reputazione sulla base del voto medio delle gare dell'utente corrente
				acc += Double.parseDouble(PortletProps.get("my-challenges-avg-score-weight"))*RatingsStatsLocalServiceUtil.getStats(CLSChallenge.class.getName(), c.getChallengeId()).getAverageScore();
			}
			catch(Exception e) {
				Logger.getLogger(this.getClass().getName()).warn("Error analyzing challenge "+c+": "+e.getMessage());
			}
		}
		
		return acc;
	}
	
	private double getIdeasRatingContribution(long ideaId){
		double pr = 0;
		try{ pr = RatingsStatsLocalServiceUtil.getStats(CLSIdeaProblemRelevant.class.getName(), ideaId).getAverageScore(); }
		catch(Exception e) { Logger.getLogger(this.getClass().getName()).debug("Issue with Problem Relevant for idea "+ideaId); }
		
		double sp = 0;
		try{ RatingsStatsLocalServiceUtil.getStats(CLSIdeaSolveProblem.class.getName(), ideaId).getAverageScore(); }
		catch(Exception e) { Logger.getLogger(this.getClass().getName()).debug("Issue with Solve Problem for idea "+ideaId); }
		
		double sr = 0;
		try{ sr = RatingsStatsLocalServiceUtil.getStats(CLSIdeaSolutionRealistic.class.getName(), ideaId).getAverageScore(); }
		catch(Exception e) { Logger.getLogger(this.getClass().getName()).debug("Issue with Solution Realistic for idea "+ideaId); }
		
		double w_pr = Double.parseDouble(PortletProps.get("problem-relevant-weight"));
		double w_sp = Double.parseDouble(PortletProps.get("solve-problem-weight"));
		double w_sr = Double.parseDouble(PortletProps.get("solution-realistic-weight"));
		
		return w_pr+pr + w_sp*sp + w_sr + sr;
	}
	
	private double getIdeasContribution(List<CLSIdea> ideas) {
		
		double ideasToMunicipalityAcc = 0.0;
		double ideasToChallengesAcc = 0.0;
		double ideasRatingAcc = 0.0;
		
		for(CLSIdea i : ideas) {
			try {
				ideasToChallengesAcc += Double.parseDouble(PortletProps.get("my-challenges-ideas-"+i.getIdeaStatus()));
				ideasRatingAcc += getIdeasRatingContribution(i.getIdeaID());
			}
			catch(Exception e) {
				Logger.getLogger(this.getClass().getName()).warn("Error analyzing idea "+i+": "+e.getMessage());
			}
		}
		
		return (ideasToChallengesAcc+ideasToMunicipalityAcc+ideasRatingAcc);
	}
	
	private double getIdeaCollaborator(long userId) throws NumberFormatException, SystemException {
		return Double.parseDouble(PortletProps.get("collaboration-weight"))*CLSCoworkerLocalServiceUtil.getCoworkersByUserId(userId).size();
	}
	
	private double getMessageVotes(long msgId) throws SystemException  {
		double acc = 0.0;
		Iterator<RatingsEntry> it = RatingsEntryLocalServiceUtil.getEntries(MBDiscussion.class.getName(), msgId).iterator();
		while(it.hasNext()){
			double likeDislike = it.next().getScore();
			if(likeDislike>0)
				acc += Double.parseDouble(PortletProps.get("comment-like-weight"));
			else
				acc += Double.parseDouble(PortletProps.get("comment-dislike-weight"));
				
		}
		return acc;
		
	}
	
	private double getMessagesContribution(long userId) throws SystemException{
		
		long aClassId = ClassNameLocalServiceUtil.getClassNameId(Artefatto.class);
		long iClassId = ClassNameLocalServiceUtil.getClassNameId(CLSIdea.class);
		long cClassId = ClassNameLocalServiceUtil.getClassNameId(CLSChallenge.class);
		
		long[] classids = {aClassId, iClassId, cClassId};
		List<MBMessage> msgs = MBMessageLocalServiceUtil.getUserDiscussionMessages(userId, 
															classids, 
															WorkflowConstants.STATUS_APPROVED, 
															QueryUtil.ALL_POS, 
															QueryUtil.ALL_POS, 
															null); //OrderBy
		
		double acc = 0.0;
		for(MBMessage msg : msgs) {
			try { 
				if(msg.getParentMessageId()>0) {
					double votes = getMessageVotes(msg.getMessageId());
					acc += votes;
				}
			}
			catch(Exception e) {
				Logger.getLogger(this.getClass().getName()).warn("Error while retrieving user's messages. "+e.getMessage());
				continue;
			}
		}
		
		return acc;
	}
	
	private double getArtefactsContribution(long userId) throws PortalException, SystemException, RemoteException {
		List<Artefatto> artefacts = ArtefattoLocalServiceUtil.findArtefattoByUserId_Status(userId, WorkflowConstants.STATUS_APPROVED);
		int n_artefatti = artefacts.size();
		double art = Double.parseDouble(PortletProps.get("artefacts-weight"))*n_artefatti;
		double avg_acc = 0.0;
		for(Artefatto a: artefacts) {
			avg_acc += RatingsStatsLocalServiceUtil.getStats(Artefatto.class.getName(), a.getArtefattoId()).getAverageScore();
		}
		double rate = avg_acc * Double.parseDouble(PortletProps.get("artefacts-rating-weight"));
		return art + rate;
		
	}
	
	private double getUserReputation(User u) {
		double userReputation = 0.0;
		
		try { 
			//Estraggo le gare pubblicate dall'utente corrente u
			List<CLSChallenge> challenges = getChallengesAuthor(u.getUserId());
			userReputation += getChallengesContribution(challenges);
			
		} 
		catch (Exception e) { 
			Logger.getLogger(this.getClass().getName()).error("Error while analyzing "+u.getScreenName()+"'s challenges");
			e.printStackTrace();
		}
		
		try {  
			//Estraggo le idee pubblicate dall'utente corrente u
			List<CLSIdea> ideas = getIdeaAuthor(u.getUserId());
			userReputation += getIdeasContribution(ideas);
		} 
		catch (SystemException e) { 
			Logger.getLogger(this.getClass().getName()).error("Error while analyzing "+u.getScreenName()+"'s ideas");
			e.printStackTrace(); 
		}
		
		try {  
			//Aggiorno la reputazione in base al numero di idee cui l'utente corrente collabora
			userReputation += getIdeaCollaborator(u.getUserId());
		} 
		catch (Exception e) { 
			Logger.getLogger(this.getClass().getName()).error("Error while analyzing "+u.getScreenName()+"'s collaborations");
			e.printStackTrace(); 
		}
		
		try {  
			//Aggiorno la reputazione in base al numero di like/dislike che l'utente ha ricevuto sui suoi commenti
			double commentsContrib = getMessagesContribution(u.getUserId());
			userReputation += commentsContrib;
		} 
		catch (Exception e) { 
			Logger.getLogger(this.getClass().getName()).error("Error while analyzing "+u.getScreenName()+"'s messages rating");
			e.printStackTrace(); 
		}
		
		try {  
			//Aggiorno la reputazione in base al numero di artefatti che l'utente corrente ha pubblicato
			userReputation += getArtefactsContribution(u.getUserId());
		} 
		catch (Exception e) { 
			Logger.getLogger(this.getClass().getName()).error("Error while analyzing "+u.getScreenName()+"'s artefacts");
			e.printStackTrace(); 
		}
		
		return userReputation;
	}
	
	private void storeInDB(HashMap<User, Double> map) {
		Set<User> users = map.keySet();
		for(User u: users) {
			try {
				long userId = u.getUserId();
				int punteggio = (int)Math.round(map.get(u));
				List<Reputation> r = ReputationLocalServiceUtil.getReputationByUserId(userId);
				if(r!=null && !r.isEmpty()){
					r.get(0).setReputationScore(punteggio);
					r.get(0).setModifiedDate(GregorianCalendar.getInstance().getTime());
					ReputationLocalServiceUtil.updateReputation(r.get(0));
				}
				else{
					Reputation rep = new ReputationImpl();
					rep.setUserId(userId);
					rep.setReputationScore(punteggio);
					rep.setGroupId(u.getGroupId());
					rep.setCompanyId(u.getCompanyId());
					rep.setUserName(u.getFullName());
					rep.setModifiedDate(GregorianCalendar.getInstance().getTime());
					
					ReputationLocalServiceUtil.addReputation(rep);
				}
			}
			catch(Exception e){
				Logger.getLogger(this.getClass().getName()).error(e.getMessage());
			}
		}
	}
	
	@Override
	public void receive(Message arg0) throws MessageListenerException {
		if(!Boolean.parseBoolean(PortletProps.get("job-enabled")))
			return;
		
		Logger.getLogger(this.getClass().getName()).info("Updating Reputation...");
		long start = System.currentTimeMillis();
		List<User> users;
		try { users = getClassifiableUsers(); } 
		catch (Exception e) {
			Logger.getLogger(this.getClass().getName()).error("There was an error while getting the users list. "+e.getMessage());
			e.printStackTrace();
			return;
		}
		if(users==null || users.isEmpty()) {
			Logger.getLogger(this.getClass().getName()).warn("No user found");
			return;
		}
		
		HashMap<User, Double> map = new HashMap<User, Double>();
		
		for(User u : users) {
			double rep = getUserReputation(u);
			map.put(u, rep);
		}
		
		storeInDB(map);
		//CDVNotifier.notify(map);
		
		long end = System.currentTimeMillis();
		Logger.getLogger(this.getClass().getName()).info("Update Reputation completed in "+(end-start)+"ms");
	}

}
