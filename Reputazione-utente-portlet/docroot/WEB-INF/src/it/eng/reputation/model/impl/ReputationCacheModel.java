/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.reputation.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.reputation.model.Reputation;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Reputation in entity cache.
 *
 * @author asirchia
 * @see Reputation
 * @generated
 */
public class ReputationCacheModel implements CacheModel<Reputation>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{reputationId=");
		sb.append(reputationId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", reputationScore=");
		sb.append(reputationScore);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Reputation toEntityModel() {
		ReputationImpl reputationImpl = new ReputationImpl();

		reputationImpl.setReputationId(reputationId);
		reputationImpl.setGroupId(groupId);
		reputationImpl.setCompanyId(companyId);
		reputationImpl.setUserId(userId);

		if (userName == null) {
			reputationImpl.setUserName(StringPool.BLANK);
		}
		else {
			reputationImpl.setUserName(userName);
		}

		reputationImpl.setReputationScore(reputationScore);

		if (modifiedDate == Long.MIN_VALUE) {
			reputationImpl.setModifiedDate(null);
		}
		else {
			reputationImpl.setModifiedDate(new Date(modifiedDate));
		}

		reputationImpl.resetOriginalValues();

		return reputationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		reputationId = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		reputationScore = objectInput.readInt();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(reputationId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeInt(reputationScore);
		objectOutput.writeLong(modifiedDate);
	}

	public long reputationId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public int reputationScore;
	public long modifiedDate;
}