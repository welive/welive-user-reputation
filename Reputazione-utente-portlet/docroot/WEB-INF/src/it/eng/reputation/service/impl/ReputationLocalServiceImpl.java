/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.reputation.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;

import it.eng.reputation.model.Reputation;
import it.eng.reputation.model.impl.ReputationImpl;
import it.eng.reputation.service.base.ReputationLocalServiceBaseImpl;

/**
 * The implementation of the reputation local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.reputation.service.ReputationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author asirchia
 * @see it.eng.reputation.service.base.ReputationLocalServiceBaseImpl
 * @see it.eng.reputation.service.ReputationLocalServiceUtil
 */
public class ReputationLocalServiceImpl extends ReputationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.reputation.service.ReputationLocalServiceUtil} to access the reputation local service.
	 */
	
	public Reputation addReputation(Reputation rep) throws SystemException{
		Reputation r = reputationPersistence.create(counterLocalService.increment(Reputation.class.getName()));
		
		r.setGroupId(rep.getGroupId());
		r.setCompanyId(rep.getCompanyId());
		r.setReputationScore(rep.getReputationScore());
		r.setUserId(rep.getUserId());
		r.setUserName(rep.getUserName());
		r.setModifiedDate(rep.getModifiedDate());
		
		return reputationPersistence.update(r);
		
	}
	
	public List<Reputation> getReputationByUserId(long userId) throws SystemException{
		return getReputationPersistence().findByUserId(userId);
	}
	
	public List<Reputation> getOrderedReputation(){
		
		List<Reputation> out = new ArrayList<Reputation>();
		
		try {  
			OrderByComparator orderByComparator=OrderByComparatorFactoryUtil.create(ReputationImpl.TABLE_NAME, "reputationScore", false); 

			out= getReputationPersistence().findAll(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, orderByComparator);

		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return out;
	}
	
}