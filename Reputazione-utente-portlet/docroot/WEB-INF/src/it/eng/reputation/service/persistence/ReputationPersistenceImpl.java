/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.reputation.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.reputation.NoSuchReputationException;
import it.eng.reputation.model.Reputation;
import it.eng.reputation.model.impl.ReputationImpl;
import it.eng.reputation.model.impl.ReputationModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the reputation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author asirchia
 * @see ReputationPersistence
 * @see ReputationUtil
 * @generated
 */
public class ReputationPersistenceImpl extends BasePersistenceImpl<Reputation>
	implements ReputationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ReputationUtil} to access the reputation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ReputationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationModelImpl.FINDER_CACHE_ENABLED, ReputationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationModelImpl.FINDER_CACHE_ENABLED, ReputationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationModelImpl.FINDER_CACHE_ENABLED, ReputationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationModelImpl.FINDER_CACHE_ENABLED, ReputationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] { Long.class.getName() },
			ReputationModelImpl.USERID_COLUMN_BITMASK |
			ReputationModelImpl.REPUTATIONSCORE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the reputations where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching reputations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Reputation> findByUserId(long userId) throws SystemException {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reputations where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of reputations
	 * @param end the upper bound of the range of reputations (not inclusive)
	 * @return the range of matching reputations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Reputation> findByUserId(long userId, int start, int end)
		throws SystemException {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the reputations where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of reputations
	 * @param end the upper bound of the range of reputations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching reputations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Reputation> findByUserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<Reputation> list = (List<Reputation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Reputation reputation : list) {
				if ((userId != reputation.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REPUTATION_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReputationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<Reputation>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Reputation>(list);
				}
				else {
					list = (List<Reputation>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first reputation in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reputation
	 * @throws it.eng.reputation.NoSuchReputationException if a matching reputation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation findByUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchReputationException, SystemException {
		Reputation reputation = fetchByUserId_First(userId, orderByComparator);

		if (reputation != null) {
			return reputation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReputationException(msg.toString());
	}

	/**
	 * Returns the first reputation in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching reputation, or <code>null</code> if a matching reputation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation fetchByUserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Reputation> list = findByUserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last reputation in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reputation
	 * @throws it.eng.reputation.NoSuchReputationException if a matching reputation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation findByUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchReputationException, SystemException {
		Reputation reputation = fetchByUserId_Last(userId, orderByComparator);

		if (reputation != null) {
			return reputation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReputationException(msg.toString());
	}

	/**
	 * Returns the last reputation in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching reputation, or <code>null</code> if a matching reputation could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation fetchByUserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<Reputation> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the reputations before and after the current reputation in the ordered set where userId = &#63;.
	 *
	 * @param reputationId the primary key of the current reputation
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next reputation
	 * @throws it.eng.reputation.NoSuchReputationException if a reputation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation[] findByUserId_PrevAndNext(long reputationId,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchReputationException, SystemException {
		Reputation reputation = findByPrimaryKey(reputationId);

		Session session = null;

		try {
			session = openSession();

			Reputation[] array = new ReputationImpl[3];

			array[0] = getByUserId_PrevAndNext(session, reputation, userId,
					orderByComparator, true);

			array[1] = reputation;

			array[2] = getByUserId_PrevAndNext(session, reputation, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Reputation getByUserId_PrevAndNext(Session session,
		Reputation reputation, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REPUTATION_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReputationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(reputation);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Reputation> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the reputations where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUserId(long userId) throws SystemException {
		for (Reputation reputation : findByUserId(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(reputation);
		}
	}

	/**
	 * Returns the number of reputations where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching reputations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REPUTATION_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "reputation.userId = ?";

	public ReputationPersistenceImpl() {
		setModelClass(Reputation.class);
	}

	/**
	 * Caches the reputation in the entity cache if it is enabled.
	 *
	 * @param reputation the reputation
	 */
	@Override
	public void cacheResult(Reputation reputation) {
		EntityCacheUtil.putResult(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationImpl.class, reputation.getPrimaryKey(), reputation);

		reputation.resetOriginalValues();
	}

	/**
	 * Caches the reputations in the entity cache if it is enabled.
	 *
	 * @param reputations the reputations
	 */
	@Override
	public void cacheResult(List<Reputation> reputations) {
		for (Reputation reputation : reputations) {
			if (EntityCacheUtil.getResult(
						ReputationModelImpl.ENTITY_CACHE_ENABLED,
						ReputationImpl.class, reputation.getPrimaryKey()) == null) {
				cacheResult(reputation);
			}
			else {
				reputation.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all reputations.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ReputationImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ReputationImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the reputation.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Reputation reputation) {
		EntityCacheUtil.removeResult(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationImpl.class, reputation.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Reputation> reputations) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Reputation reputation : reputations) {
			EntityCacheUtil.removeResult(ReputationModelImpl.ENTITY_CACHE_ENABLED,
				ReputationImpl.class, reputation.getPrimaryKey());
		}
	}

	/**
	 * Creates a new reputation with the primary key. Does not add the reputation to the database.
	 *
	 * @param reputationId the primary key for the new reputation
	 * @return the new reputation
	 */
	@Override
	public Reputation create(long reputationId) {
		Reputation reputation = new ReputationImpl();

		reputation.setNew(true);
		reputation.setPrimaryKey(reputationId);

		return reputation;
	}

	/**
	 * Removes the reputation with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param reputationId the primary key of the reputation
	 * @return the reputation that was removed
	 * @throws it.eng.reputation.NoSuchReputationException if a reputation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation remove(long reputationId)
		throws NoSuchReputationException, SystemException {
		return remove((Serializable)reputationId);
	}

	/**
	 * Removes the reputation with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the reputation
	 * @return the reputation that was removed
	 * @throws it.eng.reputation.NoSuchReputationException if a reputation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation remove(Serializable primaryKey)
		throws NoSuchReputationException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Reputation reputation = (Reputation)session.get(ReputationImpl.class,
					primaryKey);

			if (reputation == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchReputationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(reputation);
		}
		catch (NoSuchReputationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Reputation removeImpl(Reputation reputation)
		throws SystemException {
		reputation = toUnwrappedModel(reputation);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(reputation)) {
				reputation = (Reputation)session.get(ReputationImpl.class,
						reputation.getPrimaryKeyObj());
			}

			if (reputation != null) {
				session.delete(reputation);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (reputation != null) {
			clearCache(reputation);
		}

		return reputation;
	}

	@Override
	public Reputation updateImpl(it.eng.reputation.model.Reputation reputation)
		throws SystemException {
		reputation = toUnwrappedModel(reputation);

		boolean isNew = reputation.isNew();

		ReputationModelImpl reputationModelImpl = (ReputationModelImpl)reputation;

		Session session = null;

		try {
			session = openSession();

			if (reputation.isNew()) {
				session.save(reputation);

				reputation.setNew(false);
			}
			else {
				session.merge(reputation);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ReputationModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((reputationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						reputationModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { reputationModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		EntityCacheUtil.putResult(ReputationModelImpl.ENTITY_CACHE_ENABLED,
			ReputationImpl.class, reputation.getPrimaryKey(), reputation);

		return reputation;
	}

	protected Reputation toUnwrappedModel(Reputation reputation) {
		if (reputation instanceof ReputationImpl) {
			return reputation;
		}

		ReputationImpl reputationImpl = new ReputationImpl();

		reputationImpl.setNew(reputation.isNew());
		reputationImpl.setPrimaryKey(reputation.getPrimaryKey());

		reputationImpl.setReputationId(reputation.getReputationId());
		reputationImpl.setGroupId(reputation.getGroupId());
		reputationImpl.setCompanyId(reputation.getCompanyId());
		reputationImpl.setUserId(reputation.getUserId());
		reputationImpl.setUserName(reputation.getUserName());
		reputationImpl.setReputationScore(reputation.getReputationScore());
		reputationImpl.setModifiedDate(reputation.getModifiedDate());

		return reputationImpl;
	}

	/**
	 * Returns the reputation with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the reputation
	 * @return the reputation
	 * @throws it.eng.reputation.NoSuchReputationException if a reputation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation findByPrimaryKey(Serializable primaryKey)
		throws NoSuchReputationException, SystemException {
		Reputation reputation = fetchByPrimaryKey(primaryKey);

		if (reputation == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchReputationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return reputation;
	}

	/**
	 * Returns the reputation with the primary key or throws a {@link it.eng.reputation.NoSuchReputationException} if it could not be found.
	 *
	 * @param reputationId the primary key of the reputation
	 * @return the reputation
	 * @throws it.eng.reputation.NoSuchReputationException if a reputation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation findByPrimaryKey(long reputationId)
		throws NoSuchReputationException, SystemException {
		return findByPrimaryKey((Serializable)reputationId);
	}

	/**
	 * Returns the reputation with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the reputation
	 * @return the reputation, or <code>null</code> if a reputation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Reputation reputation = (Reputation)EntityCacheUtil.getResult(ReputationModelImpl.ENTITY_CACHE_ENABLED,
				ReputationImpl.class, primaryKey);

		if (reputation == _nullReputation) {
			return null;
		}

		if (reputation == null) {
			Session session = null;

			try {
				session = openSession();

				reputation = (Reputation)session.get(ReputationImpl.class,
						primaryKey);

				if (reputation != null) {
					cacheResult(reputation);
				}
				else {
					EntityCacheUtil.putResult(ReputationModelImpl.ENTITY_CACHE_ENABLED,
						ReputationImpl.class, primaryKey, _nullReputation);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ReputationModelImpl.ENTITY_CACHE_ENABLED,
					ReputationImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return reputation;
	}

	/**
	 * Returns the reputation with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param reputationId the primary key of the reputation
	 * @return the reputation, or <code>null</code> if a reputation with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Reputation fetchByPrimaryKey(long reputationId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)reputationId);
	}

	/**
	 * Returns all the reputations.
	 *
	 * @return the reputations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Reputation> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reputations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of reputations
	 * @param end the upper bound of the range of reputations (not inclusive)
	 * @return the range of reputations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Reputation> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the reputations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.reputation.model.impl.ReputationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of reputations
	 * @param end the upper bound of the range of reputations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of reputations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Reputation> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Reputation> list = (List<Reputation>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_REPUTATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_REPUTATION;

				if (pagination) {
					sql = sql.concat(ReputationModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Reputation>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Reputation>(list);
				}
				else {
					list = (List<Reputation>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the reputations from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Reputation reputation : findAll()) {
			remove(reputation);
		}
	}

	/**
	 * Returns the number of reputations.
	 *
	 * @return the number of reputations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_REPUTATION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the reputation persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.reputation.model.Reputation")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Reputation>> listenersList = new ArrayList<ModelListener<Reputation>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Reputation>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ReputationImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_REPUTATION = "SELECT reputation FROM Reputation reputation";
	private static final String _SQL_SELECT_REPUTATION_WHERE = "SELECT reputation FROM Reputation reputation WHERE ";
	private static final String _SQL_COUNT_REPUTATION = "SELECT COUNT(reputation) FROM Reputation reputation";
	private static final String _SQL_COUNT_REPUTATION_WHERE = "SELECT COUNT(reputation) FROM Reputation reputation WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "reputation.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Reputation exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Reputation exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ReputationPersistenceImpl.class);
	private static Reputation _nullReputation = new ReputationImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Reputation> toCacheModel() {
				return _nullReputationCacheModel;
			}
		};

	private static CacheModel<Reputation> _nullReputationCacheModel = new CacheModel<Reputation>() {
			@Override
			public Reputation toEntityModel() {
				return _nullReputation;
			}
		};
}