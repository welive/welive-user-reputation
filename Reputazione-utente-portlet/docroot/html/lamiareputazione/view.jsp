<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="it.eng.reputation.model.Reputation" %>
<%@page import="it.eng.reputation.service.ReputationLocalServiceUtil" %>
<%@page import="com.liferay.util.portlet.PortletProps" %>
<%@page import="com.liferay.portal.util.PortalUtil" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%-- Calcolo indice reputazione utente
L'indice di reputazione deve tener conto di:
--------------------------------------------------------------------
N  Descrizione																			
--------------------------------------------------------------------
1 	Post del blog																			
2	Mi piace ai post del blog										
3   Mi piace ai post del blog ricevuti								
4	Commenti ai post del blog																
5	Documenti pubblici condivisi														
6	Numero di connessioni (amici)									
7	Numero post della bacheca di Social Office						
8	Numero endorsement ricevuti 									
9	Numero endorsement rilasciati									
10	Indice di reputazione singolo del Progettario (Idea Management)						

	a  mi piace e non mi piace rilasciati sulle idee                
	b  mi piace e non mi piace rilasciati su commenti sulle idee    
	c  numero di idee                                              	
	d  numero di commenti rilasciati su idee                        
	e  numero di voti rilasciati su idee                            
--%>
<%
String current_url= request.getAttribute(WebKeys.CURRENT_URL).toString();
if(current_url.contains("my-page") || current_url.contains("mypage")){ 

	ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	User currentUser = PortalUtil.getUser(request);
	%>
 		<div id="container">
			<div id="demo">
			<%
				int position = 0;
				//boolean activity = false;
				List<Reputation> list = ReputationLocalServiceUtil.getOrderedReputation();
				Iterator<Reputation> it = list.iterator();
				Reputation rep = null;
				while(it.hasNext()){
					position++;
					rep = it.next();
					if(rep.getUserId() == currentUser.getUserId()){
						//activity= true; %>
						<table cellpadding="0" cellspacing="0" border="0" class="display">
							<thead>
								<tr>
									<th>Utente</th>
									<th>Punteggio</th>
									<th>Posizione</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td align="center"><%=currentUser.getFullName() %></td>
									<td align="center"><%= rep.getReputationScore() %></td>
									<td align="center"><%= position %></td>
								</tr>
							</tbody>
						</table>
						<%
					}
				}
 				//if(!activity){ 
 				%> 
<%-- 					<%= PortletProps.get("no-data-available-reputazione") %>																 --%>
<%-- 				<% } %>	 --%>
			</div>
		</div>
	<% }else{%>
		<div class="alert alert-error" align="justify">
			
			<%= PortletProps.get("this-application-will-only-function-when-placed-on-my-page-reputazione") %>
		</div>
	<% } %>
