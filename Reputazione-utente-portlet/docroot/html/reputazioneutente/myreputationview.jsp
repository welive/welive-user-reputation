
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="java.util.List"%>
<%@page import="it.eng.reputation.model.Reputation"%>
<%@page import="it.eng.reputation.service.ReputationLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.User" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects />

<% 	

String current_url= request.getAttribute(WebKeys.CURRENT_URL).toString();
String url_start="/web/";
int url_start_length=url_start.length();
int pos_start=current_url.indexOf(url_start);
String url_end="/so/profile";
int url_end_length=url_end.length();
int pos_end=current_url.indexOf(url_end);
int length_current_url= current_url.length();

if(!current_url.contains(url_end)){%>
	<div class="alert alert-error">
		<%= PortletProps.get("this-application-will-only-function-when-placed-on-a-user-page-reputazione") %>
	</div>
<%}
else{
	String user_str=current_url.substring(pos_start + url_start_length, length_current_url-url_end_length);
	User currUser = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(), user_str);
	double score = 0.0;
	List<Reputation> rep = ReputationLocalServiceUtil.getReputationByUserId(currUser.getUserId()); 
	if(rep!=null && !rep.isEmpty()){
		Reputation r = rep.get(0);
		if(r!=null)
			score = r.getReputationScore();
	} %>
	<div id="container">
		<div id="demo">
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="utente">
				<thead>
					<tr>
						<th width="10%">Avatar</th>
						<th width="35%">Utente</th>
						<th width="45%">Punteggio</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td align="center"><img class="avatar" src="<%= HtmlUtil.escape(currUser.getPortraitURL(themeDisplay)) %>"  width="65"></td>
						<td align="center"><%=currUser.getScreenName() %></td>
						<td align="center"><%= score %></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
<%}%>