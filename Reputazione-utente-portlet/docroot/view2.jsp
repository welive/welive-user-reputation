<%@page import="javax.portlet.PortletSession"%>
<%@page import="java.io.Serializable"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="it.eng.reputation.model.Reputation" %>
<%@page import="it.eng.reputation.service.ReputationLocalServiceUtil" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />
<%
ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
List<Reputation> list = ReputationLocalServiceUtil.getOrderedReputation();
User logged = themeDisplay.getUser();
String validPilot = "";
if(logged==null || logged.isDefaultUser()){
	 PortletSession sessionUser = renderRequest.getPortletSession();
	 validPilot = (String) sessionUser.getAttribute("LIFERAY_SHARED_pilot", PortletSession.APPLICATION_SCOPE);
}
else{
	Serializable sValidPilot = logged.getExpandoBridge().getAttribute("pilot");
	if(sValidPilot==null || sValidPilot.equals("")) validPilot = "All";
	else validPilot = sValidPilot.toString();
}

Iterator<Reputation> it = list.iterator(); 
Reputation rep = null;
User currUser = null;
String avatar = "";
String userURL = "";
%>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#utente').dataTable( {
		"aaSorting": [[ 2, "desc" ]], //ordinamento discendente di default della colonna "Punteggio"
		"aoColumns": [
			{ "bSortable": false },
			{ "aDataSort": [ 1 ] },
			{ "sType":"numeric", "aDataSort": [ 2 ] }
		] 
	} );
} );
</script>
<div id="contenitore" style="display:inline-block; width:100%;">
	<div id="demo">
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="utente">
			<thead>
				<tr>
					<th>Avatar</th>
					<th>Utente</th>
					<th>Punteggio</th>
				</tr>
			</thead>
			<tbody>
				<% while(it.hasNext()){
					try{
						rep = it.next();
						currUser = UserLocalServiceUtil.getUser(rep.getUserId());
						Serializable sPilot = currUser.getExpandoBridge().getAttribute("pilot");
						if(!validPilot.equals("All") && sPilot==null) {
							System.out.println("Current Pilot: "+sPilot);
							continue;
						}
						String pilot = sPilot.toString();
						if(pilot.equals(validPilot) || validPilot.equals("All")){
							//La replace � per errore su GA3
							avatar = HtmlUtil.escape(currUser.getPortraitURL(themeDisplay)).replace("amp;","");
							userURL = themeDisplay.getPortalURL()+"/web/"+currUser.getScreenName()+"/so/profile";
						%>
						<tr>
							<td width="15%" align="center"><img class="avatar" src="<%= avatar %>"  width="65"></td>
							<td width="70%" align="center"><a href="<%= userURL %>"><%=currUser.getFullName() %></a></td>
							<td width="15%" align="center"><%= rep.getReputationScore() %></td>
						</tr>
					<%} 
				} catch(Exception e){
						System.out.println("Errore nella visualizzazione dell'utente "+rep.getUserId());
					}
				}%>  
			</tbody>
		</table>
	</div>
</div>
